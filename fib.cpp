//
//  main.cpp
//  fib
//
//  Created by Greg Grant on 5/18/19.
//  Copyright © 2019 Gregory Grant. All rights reserved.
//

#include <chrono>
#include <cstdio>
#include <sstream>
#include <iostream>
#include <unordered_map>
#include <vector>

using namespace std;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
bool ParseArguments(
                    const int argc,
                    const char * argv[],
                    unordered_map<string, string>& ParameterMap)
{
  // parse command line arguments
  for (int iParam = 1; iParam < argc; ++iParam)
  {
    string ParameterString = argv[iParam];
    
    string Delim = ":";
    auto DelimPos = ParameterString.find(Delim);
    string Key = ParameterString.substr(0, DelimPos);
    string Value = ParameterString.substr(DelimPos + 1);
    
    // check that input argument is in map of expected arguments
    auto iMap = ParameterMap.find(Key);
    if (iMap == ParameterMap.end())
    {
      cerr
      << "ERROR: Unexpected argument received: " << Key
      << endl;
      
      return false;
    }
    else if (!ParameterMap[Key].empty())
    {
      cerr
      << "ERROR: Repeated argument received: " << Key
      << endl;
      
      return false;
    }
    else
    {
      ParameterMap[Key] = Value;
    }
  }
  
  return true;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
int FibI(const int n)
{
  int Prev2 = 0;
  int Prev1 = 1;
  int Curr = Prev1 + Prev2;
  for (int i = 2; i <= n; ++i)
  {
    Curr = Prev1 + Prev2;
    Prev2 = Prev1;
    Prev1 = Curr;
  }
  
  return Curr;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
int FibR(const int n)
{
  if (n == 0)
  {
    return 0;
  }
  else if (n == 1)
  {
    return 1;
  }
  else
  {
    return FibR(n - 1) + FibR(n - 2);
  }
  
  return 0;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
int main(int argc, const char * argv[])
{
  const int NumParameters = 2; // argc + 1
  
  // check number of arguments
  if (argc == 1)
  {
    cout
    << "Usage:"
    << endl
    << "  -n: [unsigned, [0, n]] Index of desired Fib value."
    << endl
    << "Example usage:"
    << endl
    << "  fib -n:3"
    << endl
    << "Output: 2"
    << endl;
    
    return 0;
  }
  else if (argc != NumParameters)
  {
    cerr
    << "ERROR: Expected " << NumParameters - 1
    << " arguments, received " << argc - 1
    << " arguments"
    << endl;
    
    return -1;
  }
  
  vector<string> Keys = { "-n" };
  unordered_map<string, string> ParameterMap = {
    {Keys[0], ""}
  };
  
  if (!ParseArguments(argc, argv, ParameterMap))
  {
    return -1;
  }
  
  // set parameter variables
  const int n = stoi(ParameterMap[Keys[0]]);
  
  {
    auto Start = chrono::steady_clock::now();
    auto Result = FibI(n);
    auto End = chrono::steady_clock::now();
    auto Duration = chrono::duration<double>(End - Start).count();
    
    cout
    << "FibI(" << n << "): " << Result << " in " << Duration << " ms"
    << endl;
  }
  
  {
    auto Start = chrono::steady_clock::now();
    auto Result = FibR(n);
    auto End = chrono::steady_clock::now();
    auto Duration = chrono::duration<double>(End - Start).count() * 1000.0;
    cout
    << "FibR(" << n << "): " << Result << " in " << Duration << " ms"
    << endl;
  }
  
  return 0;
}
