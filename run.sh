#!/bin/bash

# constants

BIN=fib

# build

mkdir -p build
cd build/
cmake ..
make
cp $BIN ..
cd ..

# run

echo
./$BIN -n:0
echo
./$BIN -n:1
echo
./$BIN -n:6
echo
./$BIN -n:40
echo
./$BIN -n:42
echo
./$BIN -n:45
echo
