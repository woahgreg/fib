**Build Instructions**

Install CMake 3.13 or above from https://cmake.org/download/

Run the following command: `./run.sh`

This will build the application, copy the binary to the current directory and run.
